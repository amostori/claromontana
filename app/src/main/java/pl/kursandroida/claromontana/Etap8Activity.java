package pl.kursandroida.claromontana;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Etap8Activity extends AppCompatActivity {

    @BindView(R.id.image_full)
    SubsamplingScaleImageView mImageFull;
    @BindView(R.id.image1)
    SubsamplingScaleImageView mImage1;
    @BindView(R.id.image2)
    SubsamplingScaleImageView mImage2;
    @BindView(R.id.image3)
    SubsamplingScaleImageView mImage3;
    @BindView(R.id.image4)
    SubsamplingScaleImageView mImage4;
    @BindView(R.id.image5)
    SubsamplingScaleImageView mImage5;
    @BindView(R.id.image6)
    SubsamplingScaleImageView mImage6;
    @BindView(R.id.image7)
    SubsamplingScaleImageView mImage7;
    @BindView(R.id.image8)
    SubsamplingScaleImageView mImage8;
    @BindView(R.id.image9)
    SubsamplingScaleImageView mImage9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etap8);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setMyImages();
    }

    private void setMyImages() {
        mImageFull.setImage(ImageSource.resource(R.drawable.etap8_calosc));
        mImage1.setImage(ImageSource.resource(R.drawable.etap8_1));
        mImage2.setImage(ImageSource.resource(R.drawable.etap8_2));
        mImage3.setImage(ImageSource.resource(R.drawable.etap8_3));
        mImage4.setImage(ImageSource.resource(R.drawable.etap8_4));
        mImage5.setImage(ImageSource.resource(R.drawable.etap8_5));
        mImage6.setImage(ImageSource.resource(R.drawable.etap8_6));
        mImage7.setImage(ImageSource.resource(R.drawable.etap8_7));
        mImage8.setImage(ImageSource.resource(R.drawable.etap8_8));
        mImage9.setImage(ImageSource.resource(R.drawable.etap8_9));
    }
}
