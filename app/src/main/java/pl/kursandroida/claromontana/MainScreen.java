package pl.kursandroida.claromontana;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by crimson on 02.07.17.
 */

public class MainScreen extends ListActivity {

    private List<String> mStrings = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStrings.add("Etap I");
        mStrings.add("Etap II");
        mStrings.add("Etap III");
        mStrings.add("Etap IV");
        mStrings.add("Etap V");
        mStrings.add("Etap VI");
        mStrings.add("Etap VII");
        mStrings.add("Etap VIII");
        mStrings.add("Etap IX");
        mStrings.add("Etap X");
        mStrings.add("Etap XI");
        mStrings.add("Etap XII");
        mStrings.add("Etap XIII");
        mStrings.add("Etap XIV");
        mStrings.add("Etap XV");
        mStrings.add("Etap rower");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mStrings);
        //adapter.addAll(mStrings);

        setListAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                startActivity(new Intent(this, EtapI.class));
                break;
            case 1://etap II
                startActivity(new Intent(this, EtapIIActivity.class));
                break;
            case 2://etap III
                startActivity(new Intent(this, EtapIIIActivity.class));
                break;
            case 3:
                startActivity(new Intent(this, EtapIVActivity.class));
                break;
            case 4:
                startActivity(new Intent(this, EtapVActivity.class));
                break;
            case 5:
                startActivity(new Intent(this, EtapVIActivity.class));
                break;
            case 6:
                startActivity(new Intent(this, EtapVIIActivity.class));
                break;
            case 7:
                startActivity(new Intent(this, Etap8Activity.class));
                break;
            case 8:
                startActivity(new Intent(this, EtapIX.class));
                break;
            case 9:
                startActivity(new Intent(this, EtapX.class));
                break;
            case 10:
                startActivity(new Intent(this, EtapXI.class));
                break;
            case 11:
                startActivity(new Intent(this, EtapXII.class));
                break;
            case 12:
                startActivity(new Intent(this, EtapXIII.class));
                break;
            case 13:
                startActivity(new Intent(this, EtapXIV.class));
                break;
            case 14:
                startActivity(new Intent(this, EtapXV.class));
                break;
            case 15:
                startActivity(new Intent(this, Rower.class));
                break;
        }

    }
}
