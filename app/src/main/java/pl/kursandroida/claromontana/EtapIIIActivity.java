package pl.kursandroida.claromontana;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EtapIIIActivity extends AppCompatActivity {

    @BindView(R.id.image1)
    SubsamplingScaleImageView mImage1;
    @BindView(R.id.image2)
    SubsamplingScaleImageView mImage2;
    @BindView(R.id.image3)
    SubsamplingScaleImageView mImage3;
    @BindView(R.id.image4)
    SubsamplingScaleImageView mImage4;
    @BindView(R.id.image5)
    SubsamplingScaleImageView mImage5;
    @BindView(R.id.image6)
    SubsamplingScaleImageView mImage6;
    @BindView(R.id.image7)
    SubsamplingScaleImageView mImage7;
    @BindView(R.id.image8)
    SubsamplingScaleImageView mImage8;
    @BindView(R.id.image9)
    SubsamplingScaleImageView mImage9;
    @BindView(R.id.image10)
    SubsamplingScaleImageView mImage10;
    @BindView(R.id.image11)
    SubsamplingScaleImageView mImage11;
    @BindView(R.id.image12)
    SubsamplingScaleImageView mImage12;
    @BindView(R.id.image13)
    SubsamplingScaleImageView mImage13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_etap_iii);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setMyImages();
    }

    private void setMyImages() {
        mImage1.setImage(ImageSource.resource(R.drawable.etap3_calosc));
        mImage2.setImage(ImageSource.resource(R.drawable.etap3_1));
        mImage3.setImage(ImageSource.resource(R.drawable.etap3_2));
        mImage4.setImage(ImageSource.resource(R.drawable.etap3_3));
        mImage5.setImage(ImageSource.resource(R.drawable.etap3_4));
        mImage6.setImage(ImageSource.resource(R.drawable.etap3_5));
        mImage7.setImage(ImageSource.resource(R.drawable.etap3_6));
        mImage8.setImage(ImageSource.resource(R.drawable.etap3_7));
        mImage9.setImage(ImageSource.resource(R.drawable.etap3_8));
        mImage10.setImage(ImageSource.resource(R.drawable.etap3_9));
        mImage11.setImage(ImageSource.resource(R.drawable.etap3_10));
        mImage12.setImage(ImageSource.resource(R.drawable.etap3_11));
        mImage13.setImage(ImageSource.resource(R.drawable.etap3_12));

    }

    @OnClick(R.id.image12)
    public void onViewClicked() {
    }
}
